# -*- Autoconf -*-
#
# Copyright (C) 2014-2017 Yann Pouillon
#
# This file is part of the LibGridXC software package. For license information,
# please see the COPYING file in the top-level directory of the LibGridXC source
# distribution.
#



# GXC_LIBXC_DETECT()
# ------------------
#
# Check whether the specified LibXC library is working.
#
AC_DEFUN([GXC_LIBXC_DETECT],[
  dnl Init
  gxc_libxc_default_libs="-lxcf90 -lxc"
  gxc_libxc_has_hdrs="unknown"
  gxc_libxc_has_mods="unknown"
  gxc_libxc_has_incs="unknown"
  gxc_libxc_has_libs="unknown"
  gxc_libxc_version="unknown"
  gxc_libxc_ok="unknown"

  dnl Prepare environment
  tmp_saved_CPPFLAGS="${CPPFLAGS}"
  tmp_saved_FCFLAGS="${FCFLAGS}"
  tmp_saved_LIBS="${LIBS}"
  CPPFLAGS="${CPPFLAGS} ${gxc_libxc_incs}"
  FCFLAGS="${FCFLAGS} ${gxc_libxc_incs}"
  if test "${gxc_libxc_libs}" = ""; then
    AC_MSG_CHECKING([for LibXC libraries to try])
    LIBS="${gxc_libxc_default_libs} ${LIBS}"
    AC_MSG_RESULT([${gxc_libxc_default_libs}])
  else
    LIBS="${gxc_libxc_libs} ${LIBS}"
  fi

  dnl Look for C includes
  AC_LANG_PUSH([C])
  AC_CHECK_HEADERS([xc.h xc_funcs.h],[gxc_libxc_has_hdrs="yes"],[gxc_libxc_has_hdrs="no"])
  AC_LANG_POP([C])

  dnl Look for Fortran includes
  AC_MSG_CHECKING([for LibXC Fortran modules])
  AC_LANG_PUSH([Fortran])
  AC_COMPILE_IFELSE([AC_LANG_PROGRAM([],
    [[
      use xc_f90_lib_m
    ]])], [gxc_libxc_has_mods="yes"], [gxc_libxc_has_mods="no"])
  AC_LANG_POP([Fortran])
  AC_MSG_RESULT([${gxc_libxc_has_mods}])

  dnl Check status of include files
  if test "${gxc_libxc_has_hdrs}" = "yes" -a \
          "${gxc_libxc_has_mods}" = "yes"; then
    gxc_libxc_has_incs="yes"
  else
    gxc_libxc_has_incs="no"
  fi

  dnl Check whether the Fortran wrappers work
  if test "${gxc_libxc_has_incs}" = "yes"; then
    AC_MSG_CHECKING([whether LibXC has Fortran support])
    AC_LANG_PUSH([Fortran])
    AC_LINK_IFELSE([AC_LANG_PROGRAM([],
      [[
        use xc_f90_lib_m
        integer :: i
        type(xc_f90_pointer_t) :: info
        i = xc_f90_info_number(info)
      ]])], [gxc_libxc_has_libs="yes"], [gxc_libxc_has_libs="no"])
    AC_LANG_POP([Fortran])
    AC_MSG_RESULT([${gxc_libxc_has_libs}])
  fi

  dnl Final adjustments
  if test "${gxc_libxc_has_incs}" = "yes" -a \
          "${gxc_libxc_has_libs}" = "yes"; then
    gxc_libxc_ok="yes"
    if test "${gxc_libxc_libs}" = ""; then
      gxc_libxc_libs="${gxc_libxc_default_libs}"
    fi
  else
    gxc_libxc_ok="no"
  fi

  dnl Restore environment
  CPPFLAGS="${tmp_saved_CPPFLAGS}"
  FCFLAGS="${tmp_saved_FCFLAGS}"
  LIBS="${tmp_saved_LIBS}"
]) # GXC_LIBXC_DETECT
